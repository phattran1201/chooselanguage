import { IconButton } from "material-ui";
import { Card, CardActions, CardText, CardTitle } from "material-ui/Card";
import * as colors from "material-ui/styles/colors";
import Refresh from "material-ui/svg-icons/navigation/refresh";
import Share from "material-ui/svg-icons/social/share";
import PropTypes from "prop-types";
import Radium from "radium";
import React, { Component } from "react";
@Radium
export default class Result extends Component {
  static propTypes = {
    question: PropTypes.object.isRequired,
    onAgainButtonClicked: PropTypes.func.isRequired
  };

  render() {
    return (
      <div style={styles.cardWrapper}>
        <Card style={styles.card}>
          <CardTitle style={styles.cardTitle}>
            {this.props.question.item_keys.map((item, key) => (
              <img
                key={key}
                style={styles.languageLogo}
                src={require(`../core/images/icons/${item.replace(
                  /-/g,
                  "_"
                )}@2x.png`)}
              />
            ))}
            <CardActions
              style={{ position: "absolute", right: 1, paddingTop: "2%" }}
            >
              <IconButton
                aria-label="Share"
                href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.facebook.com%2FITC.VHU"
              >
                <Share />
              </IconButton>
              <IconButton
                href="#"
                aria-label="Refresh"
                onClick={this.props.onAgainButtonClicked}
              >
                <Refresh />
              </IconButton>
            </CardActions>
          </CardTitle>
          <CardText style={styles.cardText}>
            {this.props.question.reason}
          </CardText>
        </Card>
      </div>
    );
  }
}

const styles = {
  cardWrapper: {
    display: "flex",
    flexFlow: "column",
    alignItems: "center"
  },
  card: {
    height: 150,
    borderRadius: 20,
    width: "50%",
    "@media (maxWidth: 1024px)": {
      width: "90%"
    }
  },
  cardTitle: {
    display: "flex",
    justifyContent: "center"
  },
  languageLogo: {
    // justifyContent: 'flex-start',
    height: 80,
    padding: "0px 10px 0px 10px"
  },
  cardText: {
    margin: 20,
    borderRadius: 10,
    backgroundColor: colors.cyan500,
    color: colors.white,
    fontSize: 20
  },
  cardActions: {
    backgroundColor: colors.cyan500
  },
  buttonWrapper: {
    display: "flex",
    justifyContent: "center"
  },
  button: {
    marginRight: 10
  }
};
