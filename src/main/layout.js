import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import PropTypes from "prop-types";
import { StyleRoot } from "radium";
import React, { Component } from "react";
// import * as colors from 'material-ui/styles/colors'
import Footer from "../core/components/footer";
import Header from "../core/components/header";

export default class Layout extends Component {
  static propTypes = {
    children: PropTypes.node
  };

  render() {
    return (
      <MuiThemeProvider>
        <StyleRoot>
          <div style={styles.container}>
            <Header />
            <div style={styles.contentWrapper}>{this.props.children}</div>
            <Footer />
          </div>
        </StyleRoot>
      </MuiThemeProvider>
    );
  }
}

const styles = {
  container: {
    position: "absolute",
    width: window.innerWidth,
    height: window.innerHeight,
    overflow: "auto",
    backgroundImage: `url(${require("../core/images/background.png")})`, // eslint-disable-line
    backgroundSize: "cover",
    justifyContent: "center",
    alignItems: "center"
  },
  contentWrapper: {
    display: "fixed"
  }
};
