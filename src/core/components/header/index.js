import { CardActions, IconButton } from "material-ui";
import Refresh from "material-ui/svg-icons/navigation/refresh";
import Share from "material-ui/svg-icons/social/share";
import Radium from "radium";
import React, { Component } from "react";
import Home from "material-ui/svg-icons/action/home";

// const style = {
//   Header: {
//     position: "absolute",
//     display: "flex",
//     justifyContent: "center",
//     alignItems: "center",
//     alignSelf: "center",
//     height: 40,
//     width: "100%",
//     zIndex: 9,
//     "@media (max-width: 1024px)": {
//       height: 80,
//       fontSize: 40
//     }
//   }
// };
@Radium
class Header extends Component {
  refreshPage() {
    window.location.reload();
  }

  render() {
    return (
      <CardActions
        style={{ position: "absolute", right: 10, paddingTop: "2%" }}
      >
       <IconButton
          iconStyle={{ color: "#fff" }}
          aria-label="Share"
          href="https://www.facebook.com/ITC.VHU"
        >
          <Home />
        </IconButton>
        <IconButton
          iconStyle={{ color: "#fff" }}
          aria-label="Share"
          href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.facebook.com%2FITC.VHU"
        >
          <Share />
        </IconButton>
        <IconButton
          iconStyle={{ color: "#fff" }}
          href="#"
          aria-label="Refresh"
          onClick={this.refreshPage}
        >
          <Refresh />
        </IconButton>
      </CardActions>
    );
  }
}
export default Header;
